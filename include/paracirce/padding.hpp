/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file padding.hpp
 *
 * @brief Applies the results of
 *  @unpublished{legrand:hal-03190252,
 *   TITLE = {{Algorithms to speed up the generation of stationary Gaussian
 * Random Fields with the Circulant Embedding method}}, AUTHOR = {Legrand, Simon
 * and Pichot, G{\'e}raldine and Tepakbong-Tematio, Nathanael}, URL =
 * {https://hal.inria.fr/hal-03190252}, NOTE = {working paper or preprint}, YEAR
 * = {2021}, MONTH = Apr, PDF =
 * {https://hal.inria.fr/hal-03190252/file/manuscript_CEM.pdf}, HAL_ID =
 * {hal-03190252}, HAL_VERSION = {v1},
 *  }
 * to estimate the minimum size of the domain needed to generate a GRF.
 *
 * @warning This padding estimation only function for Matern and Gaussian cases.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */
#ifndef PADDING_H_
#define PADDING_H_

#include "domain.hpp"
#include "functions.hpp"

namespace paracirce
{

/**
 * Returns the ratio of the estimated minimum length over lambda
 * $\ell_{min}/\lambda$
 * */
template <typename T, size_t DIM> T M_(T nu, T r)
{
    T C1{};
    T C2{};
    if constexpr (DIM == 2)
    {
        C1 = 1.36;
        C2 = 1.71;
    }
    else
    {
        C1 = 2.8;
        C2 = 2.53 * std::pow(nu, -0.31);
    }
    return C1 +
           C2 * std::pow(nu, 0.5) * std::log(std::max(r, std::pow(nu, 0.5)));
};

// template <typename T, size_t DIM>
// T G_(T L, T lc, size_t N)
// {
//     T a{};
//     if constexpr (DIM==2)
//         a = 5.85;
//     else
//         a = 6.;
//     T c = a / 4.;
//     T tmp = lc * N / L;
//     if (tmp <= 4.)
//         return std::max(L, c*tmp*lc);
//     else
//         return std::max(L, a*lc);
// }

/**
 * Returns the ratio of the estimated minimum length over lambda
 * $\ell_{min}/\lambda$
 * */
template <typename T, size_t DIM> T G_(T L, T lc, size_t m)
{
    T alpha1{};
    T alpha2{};
    if constexpr (DIM == 2)
    {
        alpha1 = 8.69e-3;
        alpha2 = 8.09;
    }
    else
    {
        alpha1 = 0.0176;
        alpha2 = 8.2338;
    }
    T ratio_lambda_h0 = lc * m / L;
    return alpha1 * ratio_lambda_h0 + alpha2;
}

/**
 * @brief Implementation of the Graham criteria to accelerate the
 * padding computation.
 * */
template <typename T, size_t DIM>
DiscretDomain<T, DIM> padding_estimation(const DiscretDomain<T, DIM> &init_dom,
                                         Matern<T, DIM> cf)
{
    auto lc      = cf.lc();
    auto N       = nb_samples(init_dom);
    auto h0      = steps(init_dom);
    auto nu      = cf.nu();

    auto offsets = std::array<std::pair<long long, long long>, DIM>{};
    T N_p{};
    T r{};
    for (size_t i = 0; i < DIM; i++)
    {
        r   = lc[i] / h0[i];
        N_p = std::max(N[i] - 1, (size_t)ceil(M_<T, DIM>(nu, r) * r)) + 1;
        offsets[i] = {0, N_p - N[i]};
    }
    return resize(init_dom, offsets);
};

template <typename T, size_t DIM>
DiscretDomain<T, DIM> padding_estimation(const DiscretDomain<T, DIM> &init_dom,
                                         Gaussian<T, DIM> cf)
{
    auto lc      = cf.lc();
    auto N       = nb_samples(init_dom);
    auto h0      = steps(init_dom);
    auto L       = lengths(init_dom);

    auto offsets = std::array<std::pair<long long, long long>, DIM>{};
    T r{};
    T N_p{};
    for (size_t i = 0; i < DIM; i++)
    {
        r   = lc[i] / h0[i];
        N_p = std::max(N[i] - 1,
                       (size_t)ceil(G_<T, DIM>(L[i], lc[i], N[i] - 1) * r)) +
              1;
        offsets[i] = {0, N_p - N[i]};
    }
    return resize(init_dom, offsets);
};
} // namespace paracirce
#endif // PADDING_H_
