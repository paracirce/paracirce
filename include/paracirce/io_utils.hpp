/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file io_utils.hpp
 *
 * @brief Utilities for parallel IO and printings.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 * */
#ifndef _IO_UTILS
#define _IO_UTILS

#include <filesystem>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>
#include "domain.hpp"
#include "mpi_utils.hpp"

#ifdef PARACIRCE_HAS_HDF5
#include <hdf5.h>
#endif


namespace paracirce
{

  template <typename T, size_t DIM, typename Function> class GRF;
  
/**
 * @brief Write field of every process, in txt format,
 * into a single file filename.
 *
 * @warning The offset at which each process writes into the file is based
 * on its rank. Don't use with a communicator with an unordered group of
 * processes.
 *
 * @param filename : Name of the file to write in.
 * @param data : Local containers to write into filename
 * @param comm : MPI communicator
 *
 */
template <typename T>
int write_field_txt(const std::string &filename, const std::vector<T> &field,
                    mpi::Communicator comm, int nb_digits = 10)
{
    int rank = comm.rank();
    int size = comm.size();

    /** Delete file if exists */
    if (std::filesystem::exists(filename))
        std::filesystem::remove(filename);

    // Convert field into a string
    std::ostringstream field_str;
    field_str.precision(nb_digits);
    copy(field.begin(), field.end(), std::ostream_iterator<T>(field_str, "\n"));

    // Broadcast size of every local field_str.str()
    std::vector<int> field_str_sizes(size);
    int my_field_size  = field_str.str().size();
    field_str_sizes    = comm.all_gather(my_field_size);
    // MPI_Allgather(&my_field_size, 1, MPI_INT, field_str_sizes.data(), 1,
    //               MPI_INT, MPI_COMM_WORLD);

    // Compute the offset
    int idx_first_elem = std::accumulate(field_str_sizes.begin(),
                                         field_str_sizes.begin() + rank, 0);
    // Access file and write
    // @todo Create wrappers around MPI_File and MPI_Status to avoid copying
    // comm
    MPI_File file;
    MPI_Status status;
    MPI_File_open(comm.comm(), filename.data(),
                  MPI_MODE_CREATE | MPI_MODE_WRONLY | MPI_MODE_EXCL,
                  MPI_INFO_NULL, &file);

    MPI_File_write_at(file, idx_first_elem, field_str.str().data(),
                      my_field_size, MPI_CHAR, &status);
    // Seems to be a memory leak here with OpenMPI.
    MPI_File_close(&file);

    return 0;
}

/**
 * @brief Assuming the local range local_r is included in the global range
 * global_r, returns the number of samples that separates the left bound of
 * the global domain, from the left bound of the local domain.
 * @warning Both ranges are assumed to have the same step.
 */
template <typename T>
size_t get_offset(const Range<T> global_r, const Range<T> local_r)
{
  // TODO Write a predicate to test inclusion
  // OR write a new structure that allows to express the inclusion of
  // a domain into another.
  T step = global_r.step(); 
  return size_t(std::round(std::abs((global_r.l_bound() - local_r.l_bound()) / step)));
}

#ifdef PARACIRCE_HAS_HDF5
template <typename T, size_t DIM, typename Function>
int write_hdf5(const GRF<T,DIM,Function> grf, const char* file_name,
	       const char* dataset_name, mpi::Communicator comm)
{
    hsize_t count[DIM];

    auto global_dom = grf.global_dom();
    auto local_dom = grf.local_dom();
    
    // Set file access properties
    hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl_id, comm.comm(), MPI_INFO_NULL);

    auto file_id = H5Fcreate(file_name, H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);

    auto dims = paracirce::nb_samples(global_dom);

    auto filespace = H5Screate_simple(DIM,
				      reinterpret_cast<hsize_t *>(dims.data()),
				      NULL);
    auto datatype = H5Tcopy(H5T_NATIVE_DOUBLE);
    auto dataset = H5Dcreate(file_id, dataset_name,
			     datatype, filespace,
			     H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    /**
     * Each process defines dataset in memory and
     * writes it to the hyperslab in the file.
     */
    for (size_t i=0; i<DIM; i++)
    {
	count[i]=1;
    }
    count[DIM - 1] = paracirce::size(local_dom);
    
    hid_t memspace = H5Screate_simple(DIM,count,NULL);

    /**
     * Define the hyperslab in the file
     */
    auto count_hs_file = paracirce::nb_samples(grf.local_dom());
    // Offset of each local domain along each dimension is l_b/step
    // It corresponds to the offsetof the hyperslab in the filespace
    std::array<size_t, DIM> offset;
    std::transform(global_dom.begin(), global_dom.end(),
		   local_dom.begin(),
		   offset.begin(),
		   get_offset<T>);
    
    H5Sselect_hyperslab(filespace,
			H5S_SELECT_SET,
			reinterpret_cast<hsize_t *> (offset.data()),
			NULL,
			reinterpret_cast<hsize_t *>(count_hs_file.data()),
			NULL);
      
    // // HDF5 must use collective MPI_IO
    hid_t xf_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(xf_id, H5FD_MPIO_COLLECTIVE);

    H5Dwrite(dataset, H5T_NATIVE_DOUBLE, memspace, filespace, xf_id, grf.data().data());

    H5Dclose(dataset);
    H5Sclose(filespace);
    H5Sclose(memspace);
    H5Fclose(file_id);
    
    return 0;
}
#endif
  
/**
 * @brief Compute time elapsed between ref and the
 * moment of the call. Reduction is made by process
 * with rank 0.
 * @warning Introduces a synchronization, i.e. performance penalty.
 *
 * @param comm MPI communicator
 * @param ref Time reference
 *
 */
inline double measure_elapsed_time_since_ref(mpi::Communicator &comm, double ref)
{
    double maxtime    = 0;
    double total_time = MPI_Wtime() - ref;
    MPI_Reduce(&total_time, &maxtime, 1, MPI_DOUBLE, MPI_MAX, 0, comm.comm());
    return maxtime;
}

template <typename T>
void print_rank_n(const mpi::Communicator &comm, int r, const T msg)
{
    int rank = comm.rank();
    if (rank == r)
        std::cout << msg << '\n';
};

template <typename Container>
void print_cont_rank_n(const mpi::Communicator &comm, int r, Container data)
{
    int rank = comm.rank();
    using T  = typename Container::value_type;
    if (rank == r)
    {
        for (T elem : data)
            std::cout << elem << '\n';
    }
};

template <typename T>
void ordonnate_parallel_print(const mpi::Communicator &comm, const T msg)
{
    int rank = comm.rank();
    int size = comm.size();
    for (auto r = 0; r < size; r++)
    {
        comm.barrier();
        if (r == rank)
        {
            std::cout << msg << '\n';
        }
    }
};

template <typename Container>
void ordonnate_parallel_print_cont(const mpi::Communicator &comm,
                                   const Container &data)
{
    int rank = comm.rank();
    int size = comm.size();
    using T  = typename Container::value_type;
    for (auto r = 0; r < size; r++)
    {
        comm.barrier();
        if (r == rank)
        {
            for (T elem : data)
                std::cout << elem << '\n';
        }
    }
};

template <typename DiscretDomain>
void ordonnate_parallel_print_dom(const mpi::Communicator &comm,
                                  const DiscretDomain &dom)
{
    int rank = comm.rank();
    int size = comm.size();
    for (auto r = 0; r < size; r++)
    {
        comm.barrier();
        if (r == rank)
        {
            std::cout << dom << std::endl;
        }
    }
};
} // namespace paracirce
#endif
