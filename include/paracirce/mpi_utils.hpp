/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file mpi_utils.hpp
 *
 * @brief Environement class that handle initiazation and finalization, plus
 * light wrapper around communicators to offer a more modern MPI coding style.
 * It avoids the use of an extra scope to ensure that objects containing a
 * communicator are destroyed before MPI_Finalize.
 *
 * Inspired by BoostMPI and https://github.com/jzrake/mpi-plus (thanks jzrake).
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 *
 * @warning Internal purpose only. Should be completed to allow user access.
 */
#ifndef MPI_UTILS_H_
#define MPI_UTILS_H_

#include <algorithm>
#include <assert.h>
#include <complex>
#include <mpi.h>
#include <vector>
#include <cstdint>

namespace paracirce
{
namespace mpi
{

template <typename T>
[[nodiscard]] constexpr MPI_Datatype mpi_get_type() noexcept
{
    MPI_Datatype mpi_type = MPI_DATATYPE_NULL;

    if constexpr (std::is_same_v<T, char>)
    {
        mpi_type = MPI_CHAR;
    }
    else if constexpr (std::is_same_v<T, signed char>)
    {
        mpi_type = MPI_SIGNED_CHAR;
    }
    else if constexpr (std::is_same_v<T, unsigned char>)
    {
        mpi_type = MPI_UNSIGNED_CHAR;
    }
    else if constexpr (std::is_same_v<T, wchar_t>)
    {
        mpi_type = MPI_WCHAR;
    }
    else if constexpr (std::is_same_v<T, signed short>)
    {
        mpi_type = MPI_SHORT;
    }
    else if constexpr (std::is_same_v<T, unsigned short>)
    {
        mpi_type = MPI_UNSIGNED_SHORT;
    }
    else if constexpr (std::is_same_v<T, signed int>)
    {
        mpi_type = MPI_INT;
    }
    else if constexpr (std::is_same_v<T, unsigned int>)
    {
        mpi_type = MPI_UNSIGNED;
    }
    else if constexpr (std::is_same_v<T, signed long int>)
    {
        mpi_type = MPI_LONG;
    }
    else if constexpr (std::is_same_v<T, unsigned long int>)
    {
        mpi_type = MPI_UNSIGNED_LONG;
    }
    else if constexpr (std::is_same_v<T, signed long long int>)
    {
        mpi_type = MPI_LONG_LONG;
    }
    else if constexpr (std::is_same_v<T, unsigned long long int>)
    {
        mpi_type = MPI_UNSIGNED_LONG_LONG;
    }
    else if constexpr (std::is_same_v<T, float>)
    {
        mpi_type = MPI_FLOAT;
    }
    else if constexpr (std::is_same_v<T, double>)
    {
        mpi_type = MPI_DOUBLE;
    }
    else if constexpr (std::is_same_v<T, long double>)
    {
        mpi_type = MPI_LONG_DOUBLE;
    }
    else if constexpr (std::is_same_v<T, std::int8_t>)
    {
        mpi_type = MPI_INT8_T;
    }
    else if constexpr (std::is_same_v<T, std::int16_t>)
    {
        mpi_type = MPI_INT16_T;
    }
    else if constexpr (std::is_same_v<T, std::int32_t>)
    {
        mpi_type = MPI_INT32_T;
    }
    else if constexpr (std::is_same_v<T, std::int64_t>)
    {
        mpi_type = MPI_INT64_T;
    }
    else if constexpr (std::is_same_v<T, std::uint8_t>)
    {
        mpi_type = MPI_UINT8_T;
    }
    else if constexpr (std::is_same_v<T, std::uint16_t>)
    {
        mpi_type = MPI_UINT16_T;
    }
    else if constexpr (std::is_same_v<T, std::uint32_t>)
    {
        mpi_type = MPI_UINT32_T;
    }
    else if constexpr (std::is_same_v<T, std::uint64_t>)
    {
        mpi_type = MPI_UINT64_T;
    }
    else if constexpr (std::is_same_v<T, bool>)
    {
        mpi_type = MPI_C_BOOL;
    }
    else if constexpr (std::is_same_v<T, std::complex<float>>)
    {
        mpi_type = MPI_C_COMPLEX;
    }
    else if constexpr (std::is_same_v<T, std::complex<double>>)
    {
        mpi_type = MPI_C_DOUBLE_COMPLEX;
    }
    else if constexpr (std::is_same_v<T, std::complex<long double>>)
    {
        mpi_type = MPI_C_LONG_DOUBLE_COMPLEX;
    }

    assert(mpi_type != MPI_DATATYPE_NULL);

    return mpi_type;
}

/**
 * Environment class. Handles MPI_Init and MPI_Finalize if needed. Should be
 * declared first, just like MPI_Init.
 * */
class Environment
{
    /** True if this object called MPI_Init */
    bool init_;

    /** Thread level obtained */
    int provided_{MPI_THREAD_SINGLE};

  public:
    Environment(int &argc, char **&argv);
    Environment(int &argc, char **&argv, int required);
    ~Environment();
    int thread_level_provided() const { return provided_; }
    static bool initialized();
    static bool finalized();
    // MPI_Comm mpi_comm_world();
};

Environment::Environment(int &argc, char **&argv) : init_{false}
{
    if (!initialized())
    {
        MPI_Init(&argc, &argv);
        init_ = true;
    }
}

Environment::Environment(int &argc, char **&argv, int required) : init_{false}
{
    if (!initialized())
    {
        MPI_Init_thread(&argc, &argv, required, &provided_);
        init_ = true;
    }
    else
        MPI_Query_thread(&provided_);
}

Environment::~Environment()
{
    if (init_)
        MPI_Finalize();
}

bool Environment::initialized()
{
    int flag;
    MPI_Initialized(&flag);
    return flag != 0;
}

bool Environment::finalized()
{
    int flag;
    MPI_Finalized(&flag);
    return flag != 0;
}

class Communicator
{
    MPI_Comm comm_;

  public:
    /**
     * Default constructor, gives you MPI_COMM_NULL.
     */
    Communicator() {}

    /**
     * Constructor to interface regular MPI_Comm.
     * Doesn't ake ownership, duplicate.
     * */
    Communicator(const MPI_Comm &comm)
    {
        if (comm != MPI_COMM_NULL)
            MPI_Comm_dup(comm, &comm_);
    }
    /**
     * Copy constructor, duplicates the communicator and respects RAII.
     */
    Communicator(const Communicator &other)
    {
        if (!other.is_null())
            MPI_Comm_dup(other.comm_, &comm_);
    }

    /**
     * Move constructor, sets the other comm back to null.
     */
    Communicator(Communicator &&other)
    {
        comm_       = other.comm_;
        other.comm_ = MPI_COMM_NULL;
    }

    /**
     * Destructor, closes the communicator unless it was null.
     */
    ~Communicator() { close(); }

    /**
     * Assignment operator: closes this communicator and duplicates the other
     * one, unless the other one is null in which case sets this one to null,
     * e.g. you can reset a communicator by writing
     *
     *              comm_ = Communicator();
     *
     */
    Communicator &operator=(const Communicator &other)
    {
        close();
        if (!other.is_null())
            MPI_Comm_dup(other.comm_, &comm_);
        return *this;
    }

    /**
     * Move assignment. Steals the other communicator.
     */
    Communicator &operator=(Communicator &&other)
    {
        close();
        comm_       = other.comm_;
        other.comm_ = MPI_COMM_NULL;
        return *this;
    }

    /**
     * Getter for compatibility with native MPI.
     * @warning use only for non wrapped function, not safe!
     * */
    MPI_Comm comm() { return comm_; }

    /**
     * Close the communicator if it wasn't null.
     */
    void close()
    {
        if (!is_null())
        {
            MPI_Comm_free(&comm_);
            comm_ = MPI_COMM_NULL;
        }
    }

    /**
     * Return true if the communicator is null.
     */
    bool is_null() const { return comm_ == MPI_COMM_NULL; }

    /**
     * Return the number of ranks in the communicator. This returns zero for a
     * null communicator (whereas I think MPI implementations typically
     * produce an error).
     */
    int size() const
    {
        if (is_null())
            return 0;

        int res;
        MPI_Comm_size(comm_, &res);
        return res;
    }

    /**
     * Return the rank of the communicator. This returns -1 for a null
     * communicator (whereas I think MPI implementations typically produce an
     * error).
     */
    int rank() const
    {
        if (is_null())
            return -1;

        int res;
        MPI_Comm_rank(comm_, &res);
        return res;
    }

    /**
     * Block all ranks in the communicator at this points.
     */
    void barrier() const { MPI_Barrier(comm_); }

    // /**
    //  * Probe for an incoming message and return its status. This method
    //  blocks
    //  * until there is an incoming message to probe.
    //  */
    // Status probe(int rank=any_source, int tag=any_tag) const
    // {
    //     MPI_Status status;
    //     MPI_Probe(rank, tag, comm, &status);
    //     return status;
    // }

    // /**
    //  * Probe for an incoming message and return its status. This method will
    //  * not block, but returns a null status if there was no message to probe.
    //  */
    // Status iprobe(int rank=any_source, int tag=any_tag) const
    // {
    //     MPI_Status status;
    //     int flag;
    //     MPI_Iprobe(rank, tag, comm, &flag, &status);

    //     if (! flag)
    //     {
    //         return Status();
    //     }
    //     return status;
    // }

    // /**
    //  * Blocking-receive a message with the given source and tag. Return the
    //  * data as a string.
    //  */
    // std::string recv(int source=any_source, int tag=any_tag) const
    // {
    //     auto status = probe(source, tag);
    //     auto buf = std::string(status.count(), 0);

    //     MPI_Recv(&buf[0], buf.size(), MPI_CHAR, source, tag, comm,
    //     MPI_STATUS_IGNORE); return buf;
    // }

    // /**
    //  * Non-blocking receive a message with the given source and tag. Return a
    //  * request object that can be queried for the completion of the receive
    //  * operation. Note that the request is cancelled if allowed to go out of
    //  * scope. You should keep the request somewhere, and call test(), wait(),
    //  * or get() in a little while.
    //  */
    // Request irecv(int source=any_source, int tag=any_tag) const
    // {
    //     auto status = iprobe(source, tag);

    //     if (status.is_null())
    //     {
    //         return Request();
    //     }
    //     auto buf = std::string(status.count(), 0);

    //     MPI_Request request;
    //     MPI_Irecv(&buf[0], buf.size(), MPI_CHAR, source, tag, comm,
    //     &request);

    //     Request res;
    //     res.buffer = std::move(buf);
    //     res.request = request;
    //     return res;
    // }

    // /**
    //  * Blocking-send a string to the given rank.
    //  */
    // void send(std::string buf, int rank, int tag=0) const
    // {
    //     MPI_Send(&buf[0], buf.size(), MPI_CHAR, rank, tag, comm);
    // }

    // /**
    //  * Non-blocking send a string to the given rank. Returns a request object
    //  * that can be tested for completion or waited on. Note that the request
    //  * is cancelled if allowed to go out of scope. Also keep in mind your MPI
    //  * implementation may have chosen to buffer your message internally, in
    //  * which case the request will have completed immediately, and the
    //  * cancellation will have no effect. Therefore it is advisable to keep
    //  the
    //  * returned request object, or at least do something equivalent to:
    //  *
    //  *              auto result = comm.isend("Message!", 0).get();
    //  *
    //  * Of course this would literally be a blocking send, but you get the
    //  * idea. In practice you'll probably store the request somewhere and
    //  check
    //  * on it after a while.
    //  */
    // Request isend(std::string buf, int rank, int tag=0) const
    // {
    //     MPI_Request request;
    //     MPI_Isend(&buf[0], buf.size(), MPI_CHAR, rank, tag, comm, &request);

    //     Request res;
    //     res.buffer = std::move(buf);
    //     res.request = request;
    //     return res;
    // }

    // /**
    //  * Template version of a blocking send. You can pass any standard-layout
    //  * data type here.
    //  */
    // template <typename T>
    // void send(const T& value, int rank, int tag=0) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable"); auto buf = std::string(sizeof(T), 0);
    //     std::memcpy(&buf[0], &value, sizeof(T));
    //     send(buf, rank, tag);
    // }

    // /**
    //  * Template version of a non-blocking send. You can pass any
    //  standard-layout
    //  * data type here.
    //  */
    // template <typename T>
    // Request isend(const T& value, int rank, int tag=0) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable"); auto buf = std::string(sizeof(T), 0);
    //     std::memcpy(&buf[0], &value, sizeof(T));
    //     return isend(buf, rank, tag);
    // }

    // /**
    //  * Template version of a blocking receive. You can pass any
    //  * standard-layout data type here.
    //  */
    // template <typename T>
    // T recv(int rank, int tag=0) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable"); auto buf = recv(rank, tag);

    //     if (buf.size() != sizeof(T))
    //     {
    //         throw std::logic_error("received message has wrong size for data
    //         type");
    //     }

    //     auto value = T();
    //     std::memcpy(&value, &buf[0], sizeof(T));
    //     return value;
    // }

    // /**
    //  * Execute a bcast operation with the given rank as the root.
    //  */
    // template <typename T>
    // void bcast(int root, T& value) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable"); MPI_Bcast(&value, sizeof(T), MPI_CHAR, root,
    //     comm);
    // }

    // /**
    //  * Execute a scatter communication with the given rank as root. The i-th
    //  * index of the send buffer is received by the i-th rank. The send buffer
    //  * is ignored by all processes except the root.
    //  */
    // template <typename T>
    // T scatter(int root, const std::vector<T>& values) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable");

    //     if (root == rank() && values.size() != size())
    //     {
    //         throw std::invalid_argument("scatter send buffer must equal the
    //         comm size");
    //     }

    //     auto value = T();

    //     MPI_Scatter(
    //         &values[0], sizeof(T), MPI_CHAR,
    //         &value, sizeof(T), MPI_CHAR, root, comm);

    //     return value;
    // }

    // /**
    //  * Execute a scatter-v communication with the given rank as root. The
    //  i-th
    //  * index of the send buffer is received by the i-th rank. The send buffer
    //  * is ignored by all processes except the root.
    //  */
    // template <typename T>
    // std::vector<T> scatter(int root, const std::vector<std::vector<T>>&
    // values) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable");

    //     if (root == rank() && values.size() != size())
    //     {
    //         throw std::invalid_argument("scatter send buffer must equal the
    //         comm size");
    //     }

    //     if (rank() == root)
    //     {
    //         auto sendcounts = std::vector<int>(size());
    //         auto senddispls = std::vector<int>{0};
    //         auto sendbuf    = std::vector<T>();

    //         for (int i = 0; i < values.size(); ++i)
    //         {
    //             sendcounts[i] = values[i].size() * sizeof(T);
    //             sendbuf.insert(sendbuf.end(), values[i].begin(),
    //             values[i].end());
    //         }
    //         std::partial_sum(sendcounts.begin(), sendcounts.end() - 1,
    //         std::back_inserter(senddispls));

    //         auto recvcount = scatter(root, sendcounts);
    //         auto recvbuf   = std::vector<T>(recvcount / sizeof(T));

    //         MPI_Scatterv(
    //             &sendbuf[0], &sendcounts[0], &senddispls[0], MPI_CHAR,
    //             &recvbuf[0], recvcount, MPI_CHAR, root, comm);

    //         return recvbuf;
    //     }
    //     else
    //     {
    //         auto recvcount = scatter(root, std::vector<int>());
    //         auto recvbuf   = std::vector<T>(recvcount / sizeof(T));

    //         MPI_Scatterv(
    //             nullptr, nullptr, nullptr, MPI_CHAR,
    //             &recvbuf[0], recvcount, MPI_CHAR, root, comm);

    //         return recvbuf;
    //     }
    // }

    // /**
    //  * Execute an all-to-all communication with container of data. Each rank
    //  * sends the value at index i to rank i. The return value at index j
    //  * contains the character received from rank j.
    //  */
    // template <typename T>
    // std::vector<T> all_to_all(const std::vector<T>& sendbuf) const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable");

    //     if (sendbuf.size() != size())
    //     {
    //         throw std::invalid_argument("all_to_all send buffer must equal
    //         the comm size");
    //     }

    //     auto recvbuf = std::vector<T>(sendbuf.size(), T());

    //     MPI_Alltoall(
    //         &sendbuf[0], sendbuf.size() / size() * sizeof(T), MPI_CHAR,
    //         &recvbuf[0], recvbuf.size() / size() * sizeof(T), MPI_CHAR,
    //         comm);

    //     return recvbuf;
    // }

    template <typename T> T reduce(const T &value, int root, MPI_Op op)
    {
        T recvbuf{};
        MPI_Reduce(&value, &recvbuf, 1, mpi_get_type<T>(), op, root, comm_);
        return recvbuf;
    }

    template <typename T> T scatter(const T &value, int root) const
    {
        T recvbuf{};
        MPI_Scatter(&value, 1, mpi_get_type<T>(), &recvbuf, 1,
                    mpi_get_type<T>(), root, comm_);
        return recvbuf;
    }

    /**
     * Execute an all-gather communication with data of the given scalar type.
     * The returned vector contains the value provided by process j at int
     * j-th index.
     */
    template <typename T> std::vector<T> all_gather(const T &value) const
    {
        static_assert(std::is_trivially_copyable<T>::value,
                      "type is not trivially copyable");

        auto recvbuf = std::vector<T>(size(), T());
        MPI_Allgather(&value, 1, mpi_get_type<T>(), &recvbuf[0], 1,
                      mpi_get_type<T>(), comm_);
        return recvbuf;
    }

    /**
     * Execute an all-gather communication with data of the given scalar type.
     * The returned vector contains the value provided by process j at int
     * j-th index.
     */
    template <typename T> T all_reduce(const T &value, MPI_Op op) const
    {
        T recvbuf;
        MPI_Allreduce(&value, &recvbuf, 1, mpi_get_type<T>(), op, comm_);
        return recvbuf;
    }

    // /**
    //  * Execute an all-gather-v communication. This is a generalization of the
    //  * above, where each rank broadcasts to all others a container of items.
    //  * The size of the container to be broadcasted need not be the same on
    //  * every rank. The container broadcasted by rank j is returned in the
    //  j-th
    //  * index of the vector returned by this function.
    //  */
    // template <typename T>
    // std::vector<std::vector<T>> all_gather(const std::vector<T>& sendbuf)
    // const
    // {
    //     static_assert(std::is_trivially_copyable<T>::value, "type is not
    //     trivially copyable");

    //     auto recvcounts = all_gather(int(sendbuf.size() * sizeof(T)));
    //     auto recvdispls = std::vector<int>{0};
    //     std::partial_sum(recvcounts.begin(), recvcounts.end(),
    //     std::back_inserter(recvdispls));

    //     auto recvbuf = std::vector<T>(recvdispls.back() / sizeof(T));

    //     MPI_Allgatherv(
    //         &sendbuf[0], sendbuf.size() * sizeof(T), MPI_CHAR,
    //         &recvbuf[0], &recvcounts[0], &recvdispls[0], MPI_CHAR, comm);

    //     auto res = std::vector<std::vector<T>>(size());
    //     auto recv = recvbuf.begin();

    //     for (int i = 0; i < res.size(); ++i)
    //     {
    //         res[i].resize(recvcounts[i] / sizeof(T));

    //         for (int j = 0; j < res[i].size(); ++j)
    //         {
    //             res[i][j] = *recv++;
    //         }
    //     }
    //     return res;
    // }
};

template <typename T>
bool is_real_part_less(const std::complex<T> a, const std::complex<T> b)
{
    return a.real() < b.real();
}

template <typename T>
T get_min_parallel(mpi::Communicator &comm,
                   const std::vector<std::complex<T>> d)
{
    auto min = std::numeric_limits<T>::max();
    if (!d.empty())
        min = (*std::min_element(d.begin(), d.end(), is_real_part_less<T>))
                  .real();
    T global_min = 0;
    MPI_Allreduce(&min, &global_min, 1, mpi_get_type<T>(), MPI_MIN,
                  comm.comm());
    return global_min;
}

template <typename T>
T get_max_parallel(mpi::Communicator &comm,
                   const std::vector<std::complex<T>> d)
{
    auto max = std::numeric_limits<T>::min();
    if (!d.empty())
        max = (*std::max_element(d.begin(), d.end(), is_real_part_less<T>))
                  .real();
    T global_max = 0;
    MPI_Allreduce(&max, &global_max, 1, mpi_get_type<T>(), MPI_MAX,
                  comm.comm());
    return global_max;
}
} // namespace mpi
} // namespace paracirce
#endif // MPI_UTILS_H_
