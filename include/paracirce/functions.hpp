/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file functions.hpp
 *
 * @brief Functors representing correlation functions.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */
#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <array>
#include <complex>
#include <iostream>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

namespace paracirce
{

/**
 * @brief Exponential function
 *
 * @todo corriger la formule Latex
 *
 * @details \f$\xi(x,y,z) = e^{-\sqrt{\frac{x^2}{\lambda_x}+
 * \frac{y^2}{\lambda_y}+\frac{z^2}{\lambda_z}}}\f$,
 * where \f$\lambda_x, \lambda_y, \lambda_z\f$ are the
 * correlation lengths along respectively x, y and z axis.
 */
template <typename T, size_t DIM> class Exponential
{
  private:
    const std::array<T, DIM> lc_;

  public:
    /**
     * @param lc_ = \f$\lambda_\f$
     */
    Exponential(const std::array<T, DIM> lc) : lc_(lc)
    {
        for (const auto &l : lc)
        {
            if (l <= 0.)
            {
                std::cout
                    << "ERROR: Correlation lengths must be strictly positive."
                    << std::endl;
                exit(-1);
            }
        }
    }

    T operator()(const std::array<T, DIM> &X) const
    {
        T d_res =
            std::transform_reduce(X.cbegin(), X.cend(), // First container
                                  lc_.cbegin(),         // Second container
                                  static_cast<T>(0.0),  // Reduce initial value
                                  std::plus{},          // Reduce operation
                                  [](auto a, auto b)    // transform operation
                                  { return std::pow(a, 2) / std::pow(b, 2); });
        return exp(-std::sqrt(d_res));
    }

    const std::array<T, DIM> &lc() { return lc_; }
};

/**
 * @brief Norm 1 exponential function
 *
 * @todo corriger la formule Latex
 *
 * @details \f$\xi(x,y,z) = e^{-\frac{|x|}{\lambda_x}+
 * \frac{|y|}{\lambda_y}+\frac{|z|}{\lambda_z}}\f$,
 * where \f$\lambda_x, \lambda_y, \lambda_z\f$ are the
 * correlation lengths along respectively x, y and z axis.
 */
template <typename T, size_t DIM> class Exponential_norm1
{
  private:
    const std::array<T, DIM> lc_;

  public:
    /**
     * @param lc_ = \f$\lambda_\f$
     */
    Exponential_norm1(const std::array<T, DIM> lc) : lc_(lc)
    {
        for (const auto &l : lc)
        {
            if (l <= 0.)
            {
                std::cout
                    << "ERROR: Correlation lengths must be strictly positive."
                    << std::endl;
                exit(-1);
            }
        }
    }

    T operator()(const std::array<T, DIM> &X) const
    {
        T d_res =
            std::transform_reduce(X.cbegin(), X.cend(), // First container
                                  lc_.cbegin(),         // Second container
                                  static_cast<T>(0.0),  // Reduce initial value
                                  std::plus{},          // Reduce operation
                                  [](auto a, auto b)    // transform operation
                                  { return std::abs(a) / b; });

        return exp(-d_res);
    }

    const std::array<T, DIM> &lc() { return lc_; }
};

/**
 * @brief Gaussian function
 *
 * @details $G(\vect{X}) = e^{-\sum_{i=1}^d \frac{x_i^2}{\lambda_i^2}}$,
 * with $\vect{X} = (x_i)_{i=1}^{d}$ and $\vec{\lambda} = (\lambda_i)_{i=1}^{d}$
 * being the correlation lengths along each dimension.
 *
 * */
template <typename T, size_t DIM> class Gaussian
{
  private:
    const std::array<T, DIM> lc_;

  public:
    /**
     * @param lc_ = \f$\lambda_\f$
     */
    Gaussian(std::array<T, DIM> lc) : lc_(lc)
    {
        for (const auto &l : lc)
        {
            if (l <= 0.)
            {
                std::cout
                    << "ERROR: Correlation lengths must be strictly positive."
                    << std::endl;
                exit(-1);
            }
        }
    }

    T operator()(const std::array<T, DIM> &X) const
    {
        T d_res =
            std::transform_reduce(X.cbegin(), X.cend(), // First container
                                  lc_.cbegin(),         // Second container
                                  static_cast<T>(0.0),  // Reduce initial value
                                  std::plus{},          // Reduce operation
                                  [](auto a, auto b)    // transform operation
                                  { return pow(a / b, 2); });

        return exp(-0.5 * d_res);
    }

    const std::array<T, DIM> &lc() { return lc_; }
};

/**
 * @brief Matern function
 *
 * @todo corriger la formule Latex
 *
 * @details \f$\xi(x,y,z) =
 * \frac{2^{1-\nu}}{\Gamma(\nu)}(\sqrt{2\, \nu}\frac{normp}{\lambda})^\nu
 * K_{\nu}(\sqrt{2}\frac{normp}{\lambda})\f$, where
 * \f$normp=\sqrt{x^2+y^2+z^2}\f$ and \f$\lambda\f$ is the correlation length
 * along respectively x, y and z axis (isotropic case), \f$\Gamma\f$ the Gamma
 * function and \f$K_{\nu}\f$ the modified Bessel function of second kind.
 */
template <typename T, size_t DIM> class Matern
{
  private:
    const std::array<T, DIM> lc_;
    const T nu_;

  public:
    /**
     * @param lc_ = \f$\lambda_\f$
     */
    Matern(std::array<T, DIM> lc, T nu) : lc_(lc), nu_(nu)
    {
        for (const auto &l : lc)
        {
            if (l <= 0.)
            {
                std::cout
                    << "ERROR: Correlation lengths must be strictly positive."
                    << std::endl;
                exit(-1);
            }
        }
    }

    T operator()(const std::array<T, DIM> &X) const
    {
        T d_res =
            std::transform_reduce(X.cbegin(), X.cend(), // First container
                                  lc_.cbegin(),         // Second container
                                  static_cast<T>(0.0),  // Reduce initial value
                                  std::plus{},          // Reduce operation
                                  [](auto a, auto b)    // transform operation
                                  { return pow(a / b, 2); });

        // std::cout << "d_res=" << d_res << std::endl;
        if (d_res == 0.0)
            return 1;

        const T evalp = std::sqrt(2 * nu_) * std::sqrt(d_res);

        // std::cout << "evalp=" << evalp << std::endl;

        return pow(2, (1 - nu_)) / tgamma(nu_) * pow(evalp, nu_) *
               std::cyl_bessel_k(nu_, evalp);
    }

    const std::array<T, DIM> &lc() { return lc_; }
    const T nu() { return nu_; }
};

//==============================================================================
// Functions creation
// SFINAE based pattern
//==============================================================================

// To overcome the lack of default constructor for the functions,
// add std::monostate.
template <typename T, size_t DIM>
using CorrelFunction =
    std::variant<std::monostate, Exponential<T, DIM>, Exponential_norm1<T, DIM>,
                 Gaussian<T, DIM>, Matern<T, DIM>>;

template <typename T, size_t DIM, typename... Args>
static CorrelFunction<T, DIM> create_function(const std::string id,
                                              Args &&...args)
{
    CorrelFunction<T, DIM> f{};
    // f.template is mandatory since f is template name dependent
    if (id == "exponential")
    {
        if constexpr (std::is_constructible<Exponential<T, DIM>,
                                            Args...>::value)
            f.template emplace<1>(
                Exponential<T, DIM>(std::forward<Args>(args)...));
    }
    else if (id == "exponential_norm1")
    {
        if constexpr (std::is_constructible<Exponential_norm1<T, DIM>,
                                            Args...>::value)

            f.template emplace<2>(
                Exponential_norm1<T, DIM>(std::forward<Args>(args)...));
    }
    else if (id == "gaussian")
    {
        if constexpr (std::is_constructible<Gaussian<T, DIM>, Args...>::value)
            f.template emplace<3>(
                Gaussian<T, DIM>(std::forward<Args>(args)...));
    }
    else if (id == "matern")
    {
        if constexpr (std::is_constructible<Matern<T, DIM>, Args...>::value)
            f.template emplace<4>(Matern<T, DIM>(std::forward<Args>(args)...));
    }
    else
        throw std::runtime_error(
            "No matching constructor for correlation function");
    return f;
}

inline size_t index_grid_to_vector(const size_t i, const size_t j,
                                   const size_t k, const size_t ny,
                                   const size_t nz)
{
    return (i * ny + j) * nz + k;
}

#ifndef PARACIRCE_HAS_OMP
  template <typename T, typename Function, typename Allocator>
void sampling_complex_2D(
    Function const &F, std::vector<T> const &x, std::vector<T> const &y,
    std::vector<std::complex<T>, Allocator> &data)
{
    size_t ij = 0;
    for (const auto &x_i : x)
    {
        for (const auto &y_j : y)
        {
            data[ij].real(F({x_i, y_j}));
            data[ij].imag(0.);
            ij++;
        }
    }
}

/**
 * @brief Sample a Function and return a vector of complex
 * with imaginary part set to zero.
 *
 * @details x, y, and z are unique values.
 * Example:
 * x = 0., 1., 2., 3., ...
 * y = 12., 13., 17., ...
 * z = 2.5, 3.7, 4.6, ...
 *
 * @todo À généraliser avec un paramètre variadique
 */
  template <typename T, typename Function, typename Allocator>
void sampling_complex_3D(
    Function const &F, std::vector<T> const &x, std::vector<T> const &y,
    std::vector<T> const &z,
    std::vector<std::complex<T>, Allocator> &data)
{
    size_t ijk = 0;
    for (const auto &x_i : x)
    {
        for (const auto &y_j : y)
        {
            for (const auto &z_k : z)
            {
                data[ijk].real(F({x_i, y_j, z_k}));
                data[ijk].imag(0.);
                ijk++;
            }
        }
    }
}
#else

  template <typename T, typename Function, typename Allocator>
void sampling_complex_2D(
    Function const &F, std::vector<T> const &x, std::vector<T> const &y,
    std::vector<std::complex<T>, Allocator> &data)
{
    const size_t Ny = y.size();
#pragma omp parallel for
    for (size_t i = 0; i < x.size(); i++)
    {
        for (size_t j = 0; j < Ny; j++)
        {
            size_t ij = index_grid_to_vector(i, j, 0, Ny, 1);
            data[ij].real(F({x[i], y[j]}));
            data[ij].imag(0.);
        }
    }
}

  template <typename T, typename Function, typename Allocator>
void sampling_complex_3D(
    Function const &F, std::vector<T> const &x, std::vector<T> const &y,
    std::vector<T> const &z,
    std::vector<std::complex<T>, Allocator> &data)
{
    const size_t Ny = y.size();
    const size_t Nz = z.size();
#pragma omp parallel for
    for (size_t i = 0; i < x.size(); i++)
    {
        for (size_t j = 0; j < Ny; j++)
        {
            for (size_t k = 0; k < Nz; k++)
            {
                size_t ijk = index_grid_to_vector(i, j, k, Ny, Nz);
                data[ijk].real(F({x[i], y[j], z[k]}));
                data[ijk].imag(0.);
            }
        }
    }
}
#endif

} // namespace paracirce
#endif
