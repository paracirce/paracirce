/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file rng.hpp
 *
 * @brief Wrapper around RngStream class that guarantees a permanent
 * offset into a substream. Useful in parallel to ensure that different
 * processes won't generate overlapping streams of numbers.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */
#ifndef __RNG_H_
#define __RNG_H_

#include "RngStream.h"
#include <array>
#include <complex>
#include <cstddef>

using namespace RngStreams;

namespace paracirce
{

/**
 * Wrapper class around the MRG32k3a random number generator RngStreams,
 * that divide the set of generated number into streams and substreams.
 * See https://github.com/umontreal-simul/RngStreams
 *
 * @warning Each instance of this class represents a different stream of
 * random numbers.
 * */
class Rng
{
    RngStream rng_{};

    /** offset in a substream */
    size_t offset_{0};

  public:
    Rng(const std::string name = "") : rng_{RngStream(name.c_str())} {};

    /**
     * Set initial seed of the RngStreams package. Determines which streams
     * each instantiation will represent.
     * @warning: To avoid using the same streams for different generators,
     * call only once at the beginning.
     * */
    static void set_global_seed(const std::array<unsigned long, 6> seed)
    {
        RngStream::SetPackageSeed(seed.data());
    }

    /**
     * Set the generator position into its subtream.
     * */
    void set_position(size_t offset)
    {
        offset_ = offset;
        rng_.AdvanceState(0, offset_);
    }

    /**
     * Move the generator to different substream. The offset
     * is conserved.
     * */
    void set_substream(size_t substream);

    /**
     * Return a realization of the uniform law in [0., 1.].
     * */
    double rand_uniform_0_1() { return rng_.RandU01(); };
};

inline void Rng::set_substream(size_t substream)
{
    /** Skip substreams */
    for (size_t i = 0; i < substream; i++)
        rng_.ResetNextSubstream();

    /** Set position in substream */
    set_position(offset_);
}

/**
 * Returns a complex which elements are a realization of the
 * normal law. Uses Box-Muller algorithm.
 * @param rng: RngStream object
 */
template <typename T> inline std::complex<T> std_complex_normal(Rng &rng)
{
    std::complex<T> z;
    static constexpr T two_pi = 2.0 * 3.14159265358979323846;

    T u1, u2;

    u1 = rng.rand_uniform_0_1();
    u2 = rng.rand_uniform_0_1();

    z.real(sqrt(-2.0 * log(u1)) * cos(two_pi * u2));
    z.imag(sqrt(-2.0 * log(u1)) * sin(two_pi * u2));

    return z;
}

} // namespace paracirce
#endif // __RNG_H_
