/**
 * This file is part of the Paracirce software.
 *
 * For academic usage, you can use this software under the GNU Affero General
 * Public version 3 as published by the Free Software Foundation. Read the
 * LICENSE file in the root directory of this source tree for more information.
 *
 * For commercial usage, please contact the project members to purchase this
 * software under proprietary license,
 *
 * Copyright (C) 2021, Inria
 *
 * @file fft_api.hpp
 *
 * @brief FFTW wrapper class.
 *
 * @note Moved only class. Cannot be copied due to the unique_ptr member on
 * fftw_plan_s.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 */
#ifndef FFT_API_H_
#define FFT_API_H_

#include "io_utils.hpp"
#include "mpi_utils.hpp"
#include <complex>
#include <cstddef>
#include <fftw3-mpi.h>
#include <fftw3.h>
#include <map>
#include <memory>
#include <vector>

#include <complex>
#ifdef PARACIRCE_HAS_OMP
#include <omp.h>
#endif

namespace paracirce
{
namespace fftw
{

/**
 * @brief fftw traits definition
 *
 * Allows to uniformize the interface to the fftw, whose
 * function names and type names change according to the type
 * handled.
 */
template <typename T> struct fftw_traits;

template <> struct fftw_traits<float>
{
    using FFTW_complex                                   = fftwf_complex;
    using FFTW_plan_s                                    = fftwf_plan_s;
    static constexpr decltype(&fftwf_malloc) FFTW_malloc = fftwf_malloc;
    static constexpr decltype(&fftwf_free) FFTW_free     = fftwf_free;
    static constexpr decltype(&fftwf_init_threads) FFTW_init_threads =
        fftwf_init_threads;
    static constexpr decltype(&fftwf_cleanup_threads) FFTW_cleanup_threads =
        fftwf_cleanup_threads;
    static constexpr decltype(&fftwf_alloc_complex) FFTW_alloc_complex =
        &fftwf_alloc_complex;
    static constexpr decltype(&fftwf_execute) FFTW_execute = &fftwf_execute;
    static constexpr decltype(&fftwf_destroy_plan) FFTW_destroy_plan =
        &fftwf_destroy_plan;
};
template <> struct fftw_traits<double>
{
    using FFTW_complex                                  = fftw_complex;
    using FFTW_plan_s                                   = fftw_plan_s;
    static constexpr decltype(&fftw_malloc) FFTW_malloc = fftw_malloc;
    static constexpr decltype(&fftw_free) FFTW_free     = fftw_free;
    static constexpr decltype(&fftw_init_threads) FFTW_init_threads =
        fftw_init_threads;
    static constexpr decltype(&fftw_cleanup_threads) FFTW_cleanup_threads =
        fftw_cleanup_threads;
    static constexpr decltype(&fftw_alloc_complex) FFTW_alloc_complex =
        &fftw_alloc_complex;
    static constexpr decltype(&fftw_execute) FFTW_execute = &fftw_execute;
    static constexpr decltype(&fftw_destroy_plan) FFTW_destroy_plan =
        &fftw_destroy_plan;
};

template <> struct fftw_traits<long double>
{
    using FFTW_complex                                   = fftwl_complex;
    using FFTW_plan_s                                    = fftwl_plan_s;
    static constexpr decltype(&fftwl_malloc) FFTW_malloc = fftwl_malloc;
    static constexpr decltype(&fftwl_free) FFTW_free     = fftwl_free;
    static constexpr decltype(&fftwl_init_threads) FFTW_init_threads =
        fftwl_init_threads;
    static constexpr decltype(&fftwl_cleanup_threads) FFTW_cleanup_threads =
        fftwl_cleanup_threads;
    static constexpr decltype(&fftwl_alloc_complex) FFTW_alloc_complex =
        &fftwl_alloc_complex;
    static constexpr decltype(&fftwl_execute) FFTW_execute = &fftwl_execute;
    static constexpr decltype(&fftwl_destroy_plan) FFTW_destroy_plan =
        &fftwl_destroy_plan;
};

template <typename T> struct fftw_mpi_traits;

template <> struct fftw_mpi_traits<float>
{
    static constexpr decltype(&fftwf_mpi_init) FFTW_mpi_init = &fftwf_mpi_init;
    static constexpr decltype(&fftwf_mpi_cleanup) FFTW_mpi_cleanup =
        &fftwf_mpi_cleanup;
    static constexpr decltype(&fftwf_mpi_local_size) FFTW_mpi_local_size =
        &fftwf_mpi_local_size;
    static constexpr decltype(&fftwf_mpi_local_size_transposed) FFTW_mpi_local_size_transposed =
        &fftwf_mpi_local_size_transposed;
    static constexpr decltype(&fftwf_mpi_plan_dft) FFTW_mpi_plan_dft =
        &fftwf_mpi_plan_dft;
};

template <> struct fftw_mpi_traits<double>
{
    static constexpr decltype(&fftw_mpi_init) FFTW_mpi_init = &fftw_mpi_init;
    static constexpr decltype(&fftw_mpi_cleanup) FFTW_mpi_cleanup =
        &fftw_mpi_cleanup;
    static constexpr decltype(&fftw_mpi_local_size) FFTW_mpi_local_size =
        &fftw_mpi_local_size;
    static constexpr decltype(&fftw_mpi_local_size_transposed) FFTW_mpi_local_size_transposed =
        &fftw_mpi_local_size_transposed;
    static constexpr decltype(&fftw_mpi_plan_dft) FFTW_mpi_plan_dft =
        &fftw_mpi_plan_dft;
};

template <> struct fftw_mpi_traits<long double>
{
    static constexpr decltype(&fftwl_mpi_init) FFTW_mpi_init = &fftwl_mpi_init;
    static constexpr decltype(&fftwl_mpi_cleanup) FFTW_mpi_cleanup =
        &fftwl_mpi_cleanup;
    static constexpr decltype(&fftwl_mpi_local_size) FFTW_mpi_local_size =
        &fftwl_mpi_local_size;
    static constexpr decltype(&fftwl_mpi_local_size_transposed) FFTW_mpi_local_size_transposed =
        &fftwl_mpi_local_size_transposed;
    static constexpr decltype(&fftwl_mpi_plan_dft) FFTW_mpi_plan_dft =
        &fftwl_mpi_plan_dft;
};


  
class UnknownPlannerFlag : public std::runtime_error
{
  public:
    UnknownPlannerFlag(std::string msg) : std::runtime_error(msg) {}
};

/**
 * Use the fftw allocator to ensure aligned memory and therefore the use
 * of vectorization.
 * */
template <typename C, typename T = typename C::value_type> struct fftw_allocator
{
    typedef C value_type;

    fftw_allocator() = default;
    template <typename U> fftw_allocator(fftw_allocator<U> const &) noexcept {}

    C *allocate(std::size_t n)
    {
        return (C *)fftw_traits<T>::FFTW_malloc(sizeof(C) * n);
    }
    void deallocate(C *p, size_t n) { fftw_traits<T>::FFTW_free(p); }
};

/* Wasn't clear at first, but good explanation here:
 * https://stackoverflow.com/questions/47186641/why-c-custom-allocator-needs-comparison-operators
 * */
template <typename T>
bool operator==(fftw_allocator<T> const &, fftw_allocator<T> const &)
{
    return true;
};

template <typename T>
bool operator!=(fftw_allocator<T> const &, fftw_allocator<T> const &)
{
    return false;
};

template <typename T> struct FFTW_plan_deleter
{
    using FFTW_plan_s = typename fftw_traits<T>::FFTW_plan_s;
    void operator()(FFTW_plan_s *plan)
    {
        if (plan != nullptr)
            fftw_traits<T>::FFTW_destroy_plan(plan);
#ifndef NDEBUG
        std::cout << "-- FFTW plan --" << std::endl;
#endif
    };
};

/**
 * @brief Class that wrap FFTW flags. Flags are stored as string views to
 * allow users to easily retrieve individual flags such as *TRANSPOSED*.
 * @note The class was not essential, but it makes the testing easier. It
 * replaces hard to test private methods into FFTW class by a public API.
 */
class FFTW_flags
{
    inline static const std::map<const std::string, const unsigned int>
        fftw_supported_flags_{
            {"FFTW_ESTIMATE", FFTW_ESTIMATE},
            {"FFTW_MEASURE", FFTW_MEASURE},
            {"FFTW_PATIENT", FFTW_PATIENT},
            {"FFTW_EXHAUSTIVE", FFTW_EXHAUSTIVE},
            {"FFTW_MPI_TRANSPOSED_IN", FFTW_MPI_TRANSPOSED_IN},
            {"FFTW_MPI_TRANSPOSED_OUT", FFTW_MPI_TRANSPOSED_OUT}};

    std::vector<std::string> flags_{};

public:
    FFTW_flags() = delete;
    FFTW_flags(std::vector<std::string> flags);

    std::vector<std::string> flags()
    {
        return flags_;
    };
    // Return flags as unsigned ints, as in the FFTW library.
    std::vector<unsigned int> native_flags();

    unsigned int flags_logical_OR();
};

inline FFTW_flags::FFTW_flags(const std::vector<std::string> flags)
{
    for (auto &e : flags)
    {
        auto elem_it = fftw_supported_flags_.find(e);
        if (elem_it != fftw_supported_flags_.end())
            flags_.push_back(elem_it->first);
        else
        {
            std::ostringstream os;
            os << "Unknown planner flags: " << e << ". Possible values are:\n";
            for (const auto &[key, val] : fftw_supported_flags_)
                os << "- " << key << "\n";
            throw UnknownPlannerFlag(os.str());
        }
    }
}

inline unsigned int FFTW_flags::flags_logical_OR()
{
    auto native_f = native_flags();
    return std::reduce(native_f.begin(), native_f.end(), 1u, std::logical_or());
}

inline std::vector<unsigned int> FFTW_flags::native_flags()
{
    std::vector<unsigned int> res;
    for (const auto &f : flags_)
        res.push_back(fftw_supported_flags_.at(f));
    return res;
}

/**
 * @brief Wrapper class around the fftw
 * */
template <typename T, size_t DIM> class FFTW
{
    static_assert((DIM == 2 || DIM == 3),
                  "Only 2D and 3D cases are handled");

  protected:
    mpi::Communicator comm_;

    /**
     * @brief Domain size
     * */
    const std::array<size_t, DIM> N_{};

    /**
     * @brief Direction of the transform
     * -1 Forward
     * +1 Backward
     * */
    const int direction_{-1};

#ifdef PARACIRCE_HAS_OMP
    /** The MPI level of thread support is sufficient */
    bool threads_ok_{false};
#endif
    // Number of element of the local array along x.
    ptrdiff_t local_nx_{};
    // Global index of the first element along x.
    ptrdiff_t local_x_start_{};
    /**
     * If using the FFTW_MPI_TRANSPOSED_* options, distribution of the transposed
     * data.
     */
    // Number of element of the local array along y.
    ptrdiff_t local_ny_{};
    // Global index of the first element along y.
    ptrdiff_t local_y_start_{};
    // Size of the local array allocated by the fftw
    // WARNING: May be greater than local_nx * N_[1] * N_[2]
    ptrdiff_t alloc_local_{};

    // FFTW plan
    using FFTW_plan_s = typename fftw_traits<T>::FFTW_plan_s;

    typedef std::unique_ptr<FFTW_plan_s, FFTW_plan_deleter<T>> FFTWPlan;
    FFTWPlan p_{nullptr};

    // Initialize with empty vector to avoid default constructor declaration.
    FFTW_flags flags_{{}};
  
  public:
    FFTW() = delete;
    FFTW(const mpi::Communicator &comm, int const direction,
         std::array<size_t, DIM> const &N);
    FFTW(const FFTW &)            = delete;
    FFTW &operator=(const FFTW &) = delete;
    FFTW(FFTW &&)                 = default;
    FFTW &operator=(FFTW &&)      = default;
    ~FFTW();

  bool is_planned();
    ptrdiff_t alloc_local() const { return alloc_local_; }
    ptrdiff_t local_nx() const { return local_nx_; }
    ptrdiff_t local_x_start() const { return local_x_start_; }
    ptrdiff_t local_ny() const { return local_ny_; }
    ptrdiff_t local_y_start() const { return local_y_start_; }
    void set_plan(std::vector<std::complex<T>, fftw_allocator<std::complex<T>>> &in_data,
		  std::vector<std::complex<T>, fftw_allocator<std::complex<T>>> &out_data,
		  std::vector<std::string> flags);
    void free_plan();
    void execute();
};

template <typename T, size_t DIM>
FFTW<T, DIM>::FFTW(const mpi::Communicator &comm, int const direction,
                   std::array<size_t, DIM> const &N)
    : comm_(comm), N_(N), direction_(direction)
{
#ifdef PARACIRCE_HAS_OMP
    int level_thread_support;
    MPI_Query_thread(&level_thread_support);
    threads_ok_ = level_thread_support >= MPI_THREAD_FUNNELED;
    if (threads_ok_)
        fftw_traits<T>::FFTW_init_threads();
#endif
    fftw_mpi_traits<T>::FFTW_mpi_init();
    // Calling the transposed version instead doesn't change the amount of
    // memory required for a non transposed transform.
    alloc_local_ = fftw_mpi_traits<T>::FFTW_mpi_local_size_transposed(
        DIM, reinterpret_cast<const ptrdiff_t *>(&N_[0]), comm_.comm(),
	&local_nx_, &local_x_start_, &local_ny_, &local_y_start_);
#ifndef NDEBUG
    std::cout << "++ FFTW ++" << std::endl;
#endif
}

template <typename T, size_t DIM> FFTW<T, DIM>::~FFTW()
{
    fftw_mpi_traits<T>::FFTW_mpi_cleanup();
    /** Warning: comm_ contains now MPI_COMM_NULL */
#ifdef PARACIRCE_HAS_OMP    
        if (threads_ok_)
            fftw_traits<T>::FFTW_cleanup_threads();
#endif
#ifndef NDEBUG
    std::cout << "-- FFTW --" << std::endl;
#endif
}

template <typename T, size_t DIM>
bool FFTW<T, DIM>::is_planned()
{
  if(p_ != nullptr)
    return true;
  else
    return false;
}

template <typename T, size_t DIM>
void FFTW<T, DIM>::set_plan(
    std::vector<std::complex<T>, fftw_allocator<std::complex<T>>> &in_data,
    std::vector<std::complex<T>, fftw_allocator<std::complex<T>>> &out_data,
    std::vector<std::string> flags)
{
#ifdef PARACIRCE_HAS_OMP
    if (threads_ok_)
    {
        int nt = omp_get_max_threads();
#ifndef NDEBUG
        print_rank_n(comm_, 0, "Running fftw with " + std::to_string(nt));
#endif
        fftw_plan_with_nthreads(nt);
    }
#endif
    using FFTW_complex = typename fftw_traits<T>::FFTW_complex;
    flags_             = flags;
    p_                 = FFTWPlan(fftw_mpi_traits<T>::FFTW_mpi_plan_dft(
        DIM, reinterpret_cast<const ptrdiff_t *>(&N_[0]),
        reinterpret_cast<FFTW_complex *>(&in_data[0]),
        reinterpret_cast<FFTW_complex *>(&out_data[0]), comm_.comm(),
        direction_, flags_.flags_logical_OR()));
#ifndef NDEBUG
    std::cout << "++ FFTW plan ++" << std::endl;
#endif
}

template <typename T, size_t DIM> void FFTW<T, DIM>::free_plan()
{
    if (p_)
        p_.reset(nullptr);
#ifndef NDEBUG
    std::cout << "-- FFTW plan --" << std::endl;
#endif
}

template <typename T, size_t DIM> void FFTW<T, DIM>::execute()
{
    if (!p_)
        throw std::runtime_error("FFTW object's plan not initialized.");
    fftw_traits<T>::FFTW_execute(p_.get());
}

} // namespace fftw
} // namespace paracirce
#endif
