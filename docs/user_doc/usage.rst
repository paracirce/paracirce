=================
ParaCirce usage
=================

**Paracirce** is a header only library. The floating point types that can be
used for the moment are ``double`` and ``long double``.

Data distribution
=================

Internally, **ParaCirce** follows the MPI `data distribution of the FFTW
<https://www.fftw.org/fftw3_doc/MPI-Data-Distribution.html>`_, .i.e the field is
sliced along the first dimension. In each of these blocks/slices, data are
stored in Row Major format, also called C order.

While you can stick to this ``default`` distribution, you can also specify a
custom 1D distribution (See the ``generate`` method of the generator_) of the
field among the processes.

If you are calling ParaCirce from a code using a Cartesian topology, it is
recommended to create a linear subset of processes to call ParaCirce and then
manually distribute the data over the other dimensions.

Data structures
===============

Domain
------

**DiscretDomain<T, DIM>**: A regularly sampled Cartesian domain in a
DIM-dimensional space, which points are encoded in type floating point type T.
Each dimension is represented by a Range, that doesn't store the samples but
provides a forward iterator to generate them if needed. Type aliases
``Domain2D<T>`` and ``Domain3D<T>`` are also available respectively for
``DiscretDomain<T, 2>`` and ``DiscretDomain<T, 3>``.

Correlation functions
---------------------

**ParaCirce** defines four correlation functions, with :math:`\vec{X} =
(x_i)_{i=1}^{DIM}` and :math:`\vec{\Lambda} = (\lambda_i)_{i=1}^{DIM}`

* **Exponential<T, DIM>**: :math:`E(\vec{X}, \vec{\Lambda}) =
  e^{-\sqrt{\displaystyle\sum_{i=1}^{DIM}\frac{x_i^2}{\lambda_i^2}}}`

* **Exponential_norm1<T, DIM>**: :math:`E_{norm1}(\vec{X}, \vec{\Lambda}) =
  e^{-\sqrt{\displaystyle\sum_{i=1}^{DIM}\frac{|x_i|}{\lambda_i}}}`

* **Gaussian<T, DIM>**: :math:`G(\vec{X},\vec{\Lambda}) =
  e^{\displaystyle-\frac{1}{2}\sum_{i=1}^{DIM} \frac{x_i^2}{\lambda_i^2}}`

* **Matern<T, DIM>**: :math:`M(\vec{X},\nu,
  \vec{\Lambda})=\kappa\left(\sqrt{\displaystyle\sum_{i=1}^{DIM}
  \left(\frac{x_i}{\lambda_i}\right)^2}, \nu\right),` where :math:`\displaystyle
  \kappa(r, \nu) = \frac{2^{1-\nu}}{\Gamma(\nu)} (\sqrt{2 \nu} \, r)^{\nu}
  K_{\nu}(\sqrt{2 \nu} \, r),` and :math:`K_{\nu}` is the modified Bessel
  function of the second kind.

Exponential, Exponential_norm1 and Gaussian have the same constructor arguments,
an array filled with correlation lengths:

.. code-block:: cpp

   Gaussian(std::array<T, DIM> lc)

Matérn funtion as ``nu`` as an extra parameter:

.. code-block:: cpp

    Matern(std::array<T, DIM> lc, T nu)

and they can be evaluated with an array of coordinates:

.. code-block:: cpp

    T operator()(const std::array<T, DIM>& X) const

Every Function has type aliases for 2D and 3D cases, for example ``Matern2D<T>``
or ``Exponential_norm13D<T>``.

.. generator_

Generator
---------

**GRFgenerator<T, DIM, Func>** is the main class, responsible for generating the
Gaussian fields. It is defined by a DiscretDomain<T, DIM> and a correlation
function. Note that the correlation function given as template argument is not
necessarily one provided by Paracirce, but can be a generic callable that take a
``const std::array<T, DIM> &`` in parameter. Like other data structures in
**Paracirce**, type aliases for 2D and 3D are available, ``GRFgenerator2D<T,
Func>`` and ``GRFgenerator3D<T, Func>``.

.. code-block:: cpp

    GRFgenerator<T, DIM, Function> (const mpi::Communicator &comm,
		                    const DiscretDomain<T, DIM> &d,
				    const Function &f);


Once instentiated, you can use one of the ``generate`` methods to create the GRF
field.

.. code-block:: cpp

   GRF<T, DIM, Function> generate(); GRF<T, DIM, Function> generate(size_t nx_local);
  
The first one returns a field which distribution follows the one chosen by
default by the FFTW.  The second one allows the user to choose the 1D
distribution he wants. Of course if the communicator contains :math:`N`
processes, then the condition :math:`\sum_{i=1}^{N} nx\_local_{i} = nx\_global`,
where :math:`nx\_global` is the number of samples of the global GRF along the
first dimension.

.. warning::

   If the generator fails to generate a GRF because the given domain is too
   small (see padding_), a `NegativeEigenvalue` exception is raised.
   It is safe to try/catch this exception ONLY, because every process in the
   communicator throws it and it is ensured that that no deadlock will occur.

Basic examples
-------------

Here is a simple example of use of ParaCirce library to generate a 2D isotropic
field:

.. literalinclude:: ../../examples/basic_2d/basic_2d.cpp
   :language: cpp

Note the use of `mpi::environment` to embed MPI initialization and finalization,
and a `mpi::Communicator` to handle isolation and memory management.

Users can also define their own correlation functions. The only constraint is that
the defined object must be callable with the same signature as the **ParaCirce**
provided correlation functions:

.. code-block:: cpp

    T operator()(const std::array<T, DIM>& X) const

Here is an example of use of custom user-defined correlation function:

.. literalinclude:: ../../examples/custom_function/custom_function.cpp
   :language: cpp
    
.. padding_

Padding
=======

The Circulant Embedding algorithm constraints
---------------------------------------------

The main idea of the Circulant Embedding Method (CEM) is to embed the covariance
matrix into a laregr nested block circulant matrix whose factorization can be
rapidly computed thanks to the fast Fourier transform (FFT) algorithm.
Nevertheless, this method requires that this matrix is at least positive
semidefinite. This is proved to be the case if the enclosing domain is
sufficiently large, as stated by Theorem 2.3 in [1]_ for cubic domains, and in
[2]_ for rectangular paralleloids.

While enlarging the domain until a proper size is obtained would work, [2]_
proposes a method to obtain a good estimate of the required domain.

**ParaCirce** implements this method and all Domain size for SPDness of the
covariance matrix.

.. [1] I. G. Graham, F. Y. Kuo, D. Nuyens, R. Scheichl, and I. H. Sloan.
       Analysis of Circulant Embedding Methods for Sampling Stationary Random
       Fields. SIAM Journal on Numerical Analysis, 56(3):1871–1895,
       January 2018.

.. [2] G. Pichot, S. Legrand, M. Kern, and N. Tepakbong-Tematio. How to
       initialize the Circulant Embedding method to speed up the generation of
       stationary Gaussian Random Fields?

Padding estimation
------------------

**ParaCirce** implements the accelerated padding estimation method. If you use
the Gaussian or Matérn correlation function supplied by **ParaCirce**, you can
call the functions `padding_estimation`:

.. code-block:: cpp

   DiscretDomain<T,DIM> padding_estimation(const DiscretDomain<T,DIM>& ,
   Matern<T, DIM>);

   DiscretDomain<T,DIM> padding_estimation(const DiscretDomain<T,DIM>& ,
   Gaussian<T, DIM>);

which return an estimate of the required domain.

.. warning::

   The returned ``DiscretDomain`` is an estimate, hence offers no guarantee it
   is large enough.

Once the padded DiscretDomain estimated, you can give it to a ``GRFgenerator``
to all

Example of use
--------------

.. literalinclude:: ../../examples/padding/padding.cpp
   :language: cpp

.. _utilities:


The RngStreams generator
========================

**ParaCirce** uses the `RngStreams
<https://github.com/umontreal-simul/RngStreams>`_ random number generator
(RNG). Its particularity is to divide the sequence of numbers into disjoint
streams and substreams. Every instance of ``paracirce::GRFgenerator`` has its
own stream of numbers and a complete independence from other instances and each
stream is itself divided into independent substreams.

This ensures the **reproducibility** and the **independence** of the generated
fields.

If you want to perform independant parameter studies and/or perform Monte
Carlo (MC) studies, you can use different instances of GRFgenerator (~different
streams) for each parameter modeled by a GRF, and change the substream of each
of these generators for each MC run. For two different physical parameters, this
would give something like:

RNG example of use
------------------

.. code-block:: cpp

    /** First generator uses a stream */
    auto generator1 = paracirce::GRFgenerator(...);
    /** Second generator uses another stream */
    auto generator2 = paracirce::GRFgenerator(...);
 
    std::vector<double> grf1;
    std::vector<double> grf2;
    for (size_t i=0; i<nb_MC; i++)
    {
        grf1 = generator1.generate();
	// Change substream for next MC run
	generator1.set_substream(i);
 
        ...

        grf2 = generator2.generate();
	// Change substream for next MC run
	generator2.set_substream(i);
 
        ...
    }

If you want to perform new independant studies, you can also change the global seed of
RngStreams by calling the static method

.. code-block:: cpp

  static void paracirce::GRFgenerator::set_rng_global_seed(const std::array<unsigned long, 6>);

at the beginning of the previous example. This will change the underlying stream
of each of the two generators. And to quote the documentation of the underlying
RngStreams function:

    Sets the initial seed s0 of the package to the six integers in the vector
    seed. The first three integers in the seed must all be less than m1 =
    4294967087, and not all 0; and the last three integers must all be less than
    m2 = 4294944443, and not all 0. If this method is not called, the default
    initial seed is (12345, 12345, 12345, 12345, 12345, 12345).

Data preallocation and plans specification
==========================================

``Paracirce`` relies essentially on the FFTW library. By default, the
`plans <https://www.fftw.org/fftw3_doc/Using-Plans.html#Using-Plans>`_ used
to generate a GRF are created when calling the ``generate()`` method, and
destroyed when it returns. This is convenient when trying to find the proper
amount of padding, the user just needs to specify the new padded domain, but
doesn't need to worry about data and plans allocation.

But when performing MC studies, the cost of the reallocation at each call to
``generate()`` can become prohibitive. Paracirce allows the user to manually
preallocate data and plans with the ``allocate()`` method of the
``GRFgenerator`` class. While the allocation would be freed when the generator
object is destroyed, it also possible to manually deallocate data and plans
with ``deallocate()`` method.

It is also possible for the user to specify the
`planners strategy <https://www.fftw.org/fftw3_doc/Planner-Flags.html>`_
in order to choose the most adapted strategy to perform the FFTs.
Four strategies are available, ``"FFTW_ESTIMATE"`` (default), ``"FFTW_MEASURE"``,
``"FFTW_PATIENT"`` and ``"FFTW_EXHAUSTIVE"``. They can be set with the
``planners_effort`` method of the ``GRFgenerator`` class.

.. code-block:: cpp

    void paracirce::GRFgenerator::planners_effort(const std::string &);

Note that the plans creation can take several (tens of) minutes for
``"FFTW_PATIENT"`` and ``"FFTW_EXHAUSTIVE"``, so unless you have large and
numerous generations to perform, default value ``"FFTW_ESTIMATE"`` should
fit your needs.

Here is an example of use of these functionalities to perform a MC study.
The program takes the number of MC run ``<nMC>`` to perform as a first argument,
and writes generated GRFs into the ``<output_dir>`` output directory given
as a second argument.

.. literalinclude:: ../../examples/monte_carlo/monte_carlo_2d.cpp
   :language: cpp

This example also outputs an approximation of the planning time so that you can
compare it with the generation time.

.. Other utilities
.. ===============

.. Work with a parameter file, write field in a file.
