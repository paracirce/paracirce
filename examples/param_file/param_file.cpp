/**
 * @file param_file.cpp
 * @brief Run time access to the correlation function type
 * via the use of a variant as a return type of create_function.
 *
 * @author Geraldine PICHOT and Simon LEGRAND
 * @version 0.1
 */
#include "paracirce.hpp"
#include <filesystem>
#include <nlohmann/json.hpp>

using namespace paracirce;

using json = nlohmann::json;

int main(int argc, char *argv[])
{
    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    const mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    /** Parse parameter file */
    if (argc == 1)
    {
        print_rank_n(paracirce_comm, 0, "Error: Missing parameter file");
        return (-1);
    }
    auto file_path  = std::filesystem::absolute(std::filesystem::path(argv[1]));
    auto output_dir = file_path.parent_path();

    std::ifstream param_file(file_path.generic_string());
    json j;
    param_file >> j;
    std::cout << "Field parameters:" << std::endl << j.dump(4) << std::endl;

    const std::array<size_t, 2> N  = j["N"];
    const std::array<double, 2> L  = j["L"];
    const std::string correl_type  = j["correl_type"];
    const std::array<double, 2> lc = j["lc"];

    /** 2D Domain creation */
    DiscretDomain2D<double> dom{{{0., L[0], N[0]}, {0., L[1], N[1]}}};

    /**
     * f_correl type is std::variant<std::monostate,
     *                               paracirce::Exponential,
     *                               paracirce::Exponential_norm1,
     *                               paracirce::Gaussian,
     *                               paracirce::Matern>
     * Access to the stored object is made with index.
     * */
    auto f_correl = create_function<double, 2>(correl_type, lc);

    std::vector<double> res{};
    if (f_correl.index() == 1) // Exponential
    {
        auto exp_generator = GRF(paracirce_comm, dom, std::get<1>(f_correl));
        res                = exp_generator.generate();
    }
    if (f_correl.index() == 2) // Exponential norm 1
    {
        auto exp_n_generator = GRF(paracirce_comm, dom, std::get<2>(f_correl));
        res                  = exp_n_generator.generate();
    }
    else if (f_correl.index() == 3) // Gaussian
    {
        auto gau_generator = GRF(paracirce_comm, dom, std::get<3>(f_correl));
        res                = gau_generator.generate();
    }
    else if (f_correl.index() == 4) // Matern
    {
        auto mat_generator = GRF(paracirce_comm, dom, std::get<4>(f_correl));
        res                = mat_generator.generate();
    }

    /** Write the field in a file */
    print_rank_n(paracirce_comm, 0,
                 std::string("Writing field in: ") +
                     output_dir.generic_string());
    write_field_txt(absolute(output_dir).append("field"), res, paracirce_comm);

    return 0;
}
