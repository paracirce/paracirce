/**
 * Example of use and test of the ParaCirce library. Generates a 2D field and
 * writes it in a file.
 *
 * @usage mpirun -n <nb_proc> basic_2d <output_dir>
 * */
#include <filesystem>

#include "paracirce.hpp"
#include "paracirce/io_utils.hpp"

using namespace paracirce;

int main(int argc, char *argv[])
{
    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    const mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    /** Parse output directory from command line */
    if (argc == 1)
    {
        print_rank_n(paracirce_comm, 0, "Error: Missing output directory");
        return (-1);
    }
    auto output_dir = std::filesystem::path(std::string(argv[1]));
    if (!std::filesystem::is_directory(output_dir))
    {
        print_rank_n(paracirce_comm, 0,
                     "Error: Output directory doesn't exists");
        return (-1);
    }

    /** Square cartesian grid 32x32 on [0., 31.]x[0.,31.] support */
    Domain2D<double> dom{{{0., 11., 17}, {0., 31., 32}}};

    /** Isotropic Gaussian correlation function */
    Gaussian2D<double> f_correl{{1., 2.}};

    /** Generator construction */
    GRFgenerator generator{paracirce_comm, dom, f_correl};

    /** Field generation */
    std::shared_ptr<GRF<double, 2, Gaussian2D<double>>> res;

    try
    {
        res = std::make_shared<GRF<double, 2, Gaussian2D<double>>>(
            generator.generate());
    }
    // Don't catch all exceptions!
    catch (NegativeEigenvalue &e)
    {
        print_rank_n(paracirce_comm, 0, e.what());
        exit(1);
    }
    /** Write the field in a file */
    print_rank_n(paracirce_comm, 0,
                 std::string("Writing field in: ") +
                     output_dir.generic_string());
    write_field_txt(absolute(output_dir).append("field"), res->data(),
                    paracirce_comm, 8);

    // write_hdf5(*res, "field_2D.h5", "field", paracirce_comm );
    return 0;
}
