/**
 * Example of use and test of the ParaCirce library. Generates a 3D field and
 * writes it in a file.
 *
 * @usage mpirun -n <nb_proc> basic_3d <output_dir>
 * */
#include <filesystem>

#include "paracirce.hpp"
#include "paracirce/io_utils.hpp"

using namespace paracirce;

int main(int argc, char *argv[])
{
    const mpi::Environment env(argc, argv, MPI_THREAD_FUNNELED);

    const mpi::Communicator paracirce_comm(MPI_COMM_WORLD);

    /** Parse output directory from command line */
    if (argc == 1)
    {
        print_rank_n(paracirce_comm, 0, "Error: Missing output directory");
        return (-1);
    }
    auto output_dir = std::filesystem::path(std::string(argv[1]));
    if (!std::filesystem::is_directory(output_dir))
    {
        print_rank_n(paracirce_comm, 0,
                     "Error: Output directory doesn't exists");
        return (-1);
    }

    /** Square cartesian grid 32x32x32 on [0.,63.]x[0.,63.]x[0.,63.] support */
    Domain3D<double> dom{{{0., 17., 25}, {0., 21., 30}, {0., 13., 35}}};

    /** Isotropic norm one exponential Correlation function */
    Exponential_norm13D<double> f_correl{{2., 1., 5.}};

    /** Generator construction */
    GRFgenerator generator{paracirce_comm, dom, f_correl};

    /** Field generation */
    std::shared_ptr<GRF<double, 3, Exponential_norm13D<double>>> res;
    try
    {
        res = std::make_shared<GRF<double, 3, Exponential_norm13D<double>>>(
            generator.generate());
    }
    catch (std::exception &e)
    {
        print_rank_n(paracirce_comm, 0, e.what());
        exit(1);
    }

    /** Write the field in a file */
    print_rank_n(paracirce_comm, 0,
                 std::string("Writing field in: ") +
                     output_dir.generic_string());
    write_field_txt(absolute(output_dir).append("field"), res->data(),
                    paracirce_comm, 8);

    // write_hdf5(*res, "field_3D.h5", "field", paracirce_comm);
    return 0;
}
