## =============================================================================
# Copyright 2020 Simon Legrand Géraldine Pichot
# =============================================================================
#[=======================================================================[.rst:
FindFFTW
--------

Finds the FFTW library

Original version of this file:
  Copyright (c) 2015, Wenzel Jakob
  https://github.com/wjakob/layerlab/blob/master/cmake/FindFFTW.cmake, commit 4d58bfdc28891b4f9373dfe46239dda5a0b561c6
Modifications:
  Copyright (c) 2017, Patrick Bos

Usage
^^^^^

find_package(FFTW [REQUIRED] [QUIET] [COMPONENTS component1 ... componentX] )

Result Variables
^^^^^^^^^^^^^^^^

It sets the following variables:

FFTW_FOUND                  ... true if fftw is found on the system
FFTW_[component]_LIB_FOUND  ... true if the component is found on the system (see components below)
FFTW_LIBRARIES              ... full paths to all found fftw libraries
FFTW_[component]_LIB        ... full path to one of the components (see below)
FFTW_INCLUDE_DIRS           ... fftw include directory paths

Variables for locating the FFTW
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

FFTW_USE_STATIC_LIBS        ... if true, only static libraries are found, otherwise both static and shared.
FFTW_ROOT                   ... if set, the libraries are first searched under this path (set cmake_policy
CMP0074)

Imported Targets
^^^^^^^^^^^^^^^^

This module provides the following targets, if found:

``FFTW::Float``
The FFTW library in simple precision.
``FFTW::Double``
The FFTW library in double precision.
``FFTW::LongDouble``
The FFTW library in long double precision. (Careful with the representation of this type)
``FFTW::FloatThreads``
The FFTW threads library in simple precision.
``FFTW::DoubleThreads``
The FFTW threads library in double precision.
``FFTW::LongDoubleThreads``
The FFTW threads library in long double precision.
``FFTW::FloatOpenMP``
The FFTW OpenMP library in simple precision.
``FFTW::DoubleOpenMP``
The FFTW OpenMP library in double precision.
``FFTW::LongDoubleOpenMP``
The FFTW OpenMP library in long double precision.
``FFTW::FloatMPI``
The FFTW MPI library in simple precision.
``FFTW::DoubleMPI``
The FFTW MPI library in double precision.
``FFTW::LongDoubleMPI``
The FFTW MPI library in long double precision.

#]=======================================================================]

# Some modulefiles set FFTW_DIR or FFTWDIR on computing platform.
if (NOT FFTW_ROOT AND (DEFINED ENV{FFTWDIR} OR DEFINED ENV{FFTW_DIR}))
  if (DEFINED ENV{FFTWDIR})
    set (FFTW_ROOT $ENV{FFTWDIR})
  else ()
    set (FFTW_ROOT $ENV{FFTW_DIR})
  endif ()
endif ()

# Check if we can use PkgConfig
find_package(PkgConfig QUIET)
if (PKG_CONFIG_FOUND)
  pkg_check_modules(PC_fftw3 QUIET fftw3)
endif ()

#Check whether to search static or dynamic libs
set(CMAKE_FIND_LIBRARY_SUFFIXES_SAV ${CMAKE_FIND_LIBRARY_SUFFIXES})

if (${FFTW_USE_STATIC_LIBS})
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_STATIC_LIBRARY_SUFFIX})
else ()
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_SAV})
endif()

find_library(
  FFTW_DOUBLE_LIB
  NAMES "fftw3"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_DOUBLE_THREADS_LIB
  NAMES "fftw3_threads"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_DOUBLE_OPENMP_LIB
  NAMES "fftw3_omp"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_DOUBLE_MPI_LIB
  NAMES "fftw3_mpi"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_FLOAT_LIB
  NAMES "fftw3f"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_FLOAT_THREADS_LIB
  NAMES "fftw3f_threads"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_FLOAT_OPENMP_LIB
  NAMES "fftw3f_omp"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_FLOAT_MPI_LIB
  NAMES "fftw3f_mpi"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_LONGDOUBLE_LIB
  NAMES "fftw3l"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_LONGDOUBLE_THREADS_LIB
  NAMES "fftw3l_threads"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_LONGDOUBLE_OPENMP_LIB
  NAMES "fftw3l_omp"
  PATHS ${PC_fftw3_LIBDIR})

find_library(
  FFTW_LONGDOUBLE_MPI_LIB
  NAMES "fftw3l_mpi"
  PATHS ${PC_fftw3_LIBDIR})

find_path(
  FFTW_INCLUDE_DIRS
  NAMES "fftw3.h"
  PATHS ${PC_fftw3_INCLUDEDIR})

find_path(
  FFTW_MPI_INCLUDE_DIRS
  NAMES "fftw3_mpi.h" "fftw3-mpi.h"
  PATHS ${PC_fftw3_INCLUDEDIR})

# Create targets

if (FFTW_DOUBLE_LIB)
  set(FFTW_Double_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_DOUBLE_LIB})
  add_library(FFTW::Double INTERFACE IMPORTED)
  set_target_properties(FFTW::Double
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_DOUBLE_LIB}")
else ()
  set(FFTW_Double_FOUND FALSE)
endif ()

if (FFTW_FLOAT_LIB)
  set(FFTW_Float_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_FLOAT_LIB})
  add_library(FFTW::Float INTERFACE IMPORTED)
  set_target_properties(FFTW::Float
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_FLOAT_LIB}")
else ()
  set(FFTW_Float_FOUND FALSE)
endif ()

if (FFTW_LONGDOUBLE_LIB)
  set(FFTW_LongDouble_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_LONGDOUBLE_LIB})
  add_library(FFTW::LongDouble INTERFACE IMPORTED)
  set_target_properties(FFTW::LongDouble
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_LONGDOUBLE_LIB}")
else ()
  set(FFTW_LongDouble_FOUND FALSE)
endif ()

if (FFTW_DOUBLE_THREADS_LIB)
  set(FFTW_DoubleThreads_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_DOUBLE_THREADS_LIB})
  add_library(FFTW::DoubleThreads INTERFACE IMPORTED)
  set_target_properties(FFTW::DoubleThreads
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_DOUBLETHREADS_LIB}")
else ()
  set(FFTW_DoubleThreads_FOUND FALSE)
endif ()

if (FFTW_FLOAT_THREADS_LIB)
  set(FFTW_FloatThreads_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_FLOAT_THREADS_LIB})
  add_library(FFTW::FloatThreads INTERFACE IMPORTED)
  set_target_properties(FFTW::FloatThreads
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_FLOAT_THREADS_LIB}")
else ()
  set(FFTW_FloatThreads_FOUND FALSE)
endif ()

if (FFTW_LONGDOUBLE_THREADS_LIB)
  set(FFTW_LongDoubleThreads_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_LONGDOUBLE_THREADS_LIB})
  add_library(FFTW::LongDoubleThreads INTERFACE IMPORTED)
  set_target_properties(FFTW::LongDoubleThreads
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_LONGDOUBLE_THREADS_LIB}")
else ()
  set(FFTW_LongDoubleThreads_FOUND FALSE)
endif ()

if (FFTW_DOUBLE_OPENMP_LIB)
  set(FFTW_DoubleOpenMP_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_DOUBLE_OPENMP_LIB})
  add_library(FFTW::DoubleOpenMP INTERFACE IMPORTED)
  set_target_properties(FFTW::DoubleOpenMP
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_DOUBLE_OPENMP_LIB}")
else ()
  set(FFTW_DoubleOpenMP_FOUND FALSE)
endif ()

if (FFTW_FLOAT_OPENMP_LIB)
  set(FFTW_FloatOpenMP_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_FLOAT_OPENMP_LIB})
  add_library(FFTW::FloatOpenMP INTERFACE IMPORTED)
  set_target_properties(FFTW::FloatOpenMP
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_FLOAT_OPENMP_LIB}")
else ()
  set(FFTW_FloatOpenMP_FOUND FALSE)
endif ()

if (FFTW_LONGDOUBLE_OPENMP_LIB)
  set(FFTW_LongDoubleOpenMP_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_LONGDOUBLE_OPENMP_LIB})
  add_library(FFTW::LongDoubleOpenMP INTERFACE IMPORTED)
  set_target_properties(FFTW::LongDoubleOpenMP
    PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FFTW_LONGDOUBLE_OPENMP_LIB}")
else ()
  set(FFTW_LongDoubleOpenMP_FOUND FALSE)
endif ()

if (FFTW_DOUBLE_MPI_LIB AND FFTW_DOUBLE_LIB)
  set(FFTW_DoubleMPI_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_DOUBLE_MPI_LIB} ${FFTW_DOUBLE_MPI})
  add_library(FFTW::DoubleMPI INTERFACE IMPORTED)
  set_property(TARGET FFTW::DoubleMPI PROPERTY 
    INTERFACE_INCLUDE_DIRECTORIES "${FFTW_MPI_INCLUDE_DIRS}")
  set_property(TARGET FFTW::DoubleMPI PROPERTY 
    INTERFACE_LINK_LIBRARIES "${FFTW_DOUBLE_MPI_LIB}" FFTW::Double)
else ()
  set(FFTW_DoubleMPI_FOUND FALSE)
endif ()

if (FFTW_FLOAT_MPI_LIB)
  set(FFTW_FloatMPI_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_FLOAT_MPI_LIB})
  add_library(FFTW::FloatMPI INTERFACE IMPORTED)
  set_property(TARGET FFTW::FloatMPI PROPERTY 
    INTERFACE_INCLUDE_DIRECTORIES "${FFTW_MPI_INCLUDE_DIRS}")
  set_property(TARGET FFTW::FloatMPI PROPERTY 
    INTERFACE_LINK_LIBRARIES "${FFTW_FLOAT_MPI_LIB}" FFTW::Float)
else ()
  set(FFTW_FloatMPI_FOUND FALSE)
endif ()

if (FFTW_LONGDOUBLE_MPI_LIB AND FFTW_LONGDOUBLE_LIB)
  set(FFTW_LongDoubleMPI_FOUND TRUE)
  list(APPEND FFTW_LIBRARIES ${FFTW_LONGDOUBLE_MPI_LIB})
  add_library(FFTW::LongDoubleMPI INTERFACE IMPORTED)
  set_property(TARGET FFTW::LongDoubleMPI PROPERTY 
    INTERFACE_INCLUDE_DIRECTORIES "${FFTW_MPI_INCLUDE_DIRS}")
  set_property(TARGET FFTW::LongDoubleMPI PROPERTY 
    INTERFACE_LINK_LIBRARIES "${FFTW_LONGDOUBLE_MPI_LIB}" FFTW::LongDouble)
else ()
  set(FFTW_LongDoubleMPI_FOUND FALSE)
endif ()
#--------------------------------------- end components

set( CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_SAV} )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(FFTW
  REQUIRED_VARS FFTW_INCLUDE_DIRS
  HANDLE_COMPONENTS
  )

mark_as_advanced(
  FFTW_INCLUDE_DIRS
  FFTW_MPI_INCLUDE_DIRS
  FFTW_LIBRARIES
  FFTW_FLOAT_LIB
  FFTW_DOUBLE_LIB
  FFTW_LONGDOUBLE_LIB
  FFTW_FLOAT_THREADS_LIB
  FFTW_DOUBLE_THREADS_LIB
  FFTW_LONGDOUBLE_THREADS_LIB
  FFTW_FLOAT_OPENMP_LIB
  FFTW_DOUBLE_OPENMP_LIB
  FFTW_LONGDOUBLE_OPENMP_LIB
  FFTW_FLOAT_MPI_LIB
  FFTW_DOUBLE_MPI_LIB
  FFTW_LONGDOUBLE_MPI_LIB
  )
