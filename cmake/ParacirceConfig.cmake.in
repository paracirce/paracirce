@PACKAGE_INIT@

include(CMakeFindDependencyMacro)

# Add current file directory to modules search path
# to find FindFFTW.cmake
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})
list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_LIST_DIR}/..)

find_dependency(RngStreams CONFIG REQUIRED)
find_dependency(MPI REQUIRED COMPONENTS CXX)
find_dependency(Threads REQUIRED)

if(@PARACIRCE_USE_OMP@)
  find_dependency(OpenMP)
endif()

if(@PARACIRCE_JSON_LOGGING@)
  find_dependency(nlohmann_json)
endif()

find_dependency(FFTW MODULE REQUIRED COMPONENTS DoubleMPI)
if(@EXTENDED_PRECISION@)
  find_dependency(FFTW MODULE REQUIRED COMPONENTS LongDouble LongDoubleMPI)
  if(@PARACIRCE_USE_OMP@ AND OpenMP_CXX_FOUND)
    find_dependency(FFTW MODULE REQUIRED COMPONENTS LongDoubleOpenMP)
  endif()
endif()
if(@SIMPLE_PRECISION@)
  find_dependency(FFTW MODULE REQUIRED COMPONENTS Float FloatMPI)
  if(@PARACIRCE_USE_OMP@ AND OpenMP_CXX_FOUND)
    find_dependency(FFTW MODULE REQUIRED COMPONENTS FloatOpenMP)
  endif()
endif()

if(@PARACIRCE_USE_HDF5@)
  find_dependency(HDF5 REQUIRED)
  set(PARACIRCE_HAS_HDF5 ON)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/ParacirceTargets.cmake)
