# Continous integration #### Set EXEC_PATH and configure ci/ci.py.in

# Path separator definition
file(TO_NATIVE_PATH "/" _sep)

set(CMAKE_SUPPRESS_REGENERATION ON)
cmake_policy(SET CMP0074 NEW)

if(CMAKE_CONFIGURATION_TYPES)
  # i.e. Visual Studio or Xcode Definition of available build configurations,
  # Release and Debug.
  set(CMAKE_CONFIGURATION_TYPES "Release;Debug")
else()
  # For single configuration generators, default build type is Release.
  include(buildType)
endif()
