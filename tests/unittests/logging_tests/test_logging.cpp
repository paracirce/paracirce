#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "paracirce/logging.hpp"
#include <fstream>
#include <filesystem>

using json = nlohmann::json;

TEST_CASE("Log file content")
{
    json j;
    std::ifstream i;
    std::filesystem::path log_file("test.log");

    Logger::Instance()("val", 12);
    Logger::Instance().write(log_file);
    i.open(log_file);
    if(i.is_open())
    {
        i >> j;
        i.close();
        std::filesystem::remove(log_file);
    }
    REQUIRE(j["val"] == 12);
}
