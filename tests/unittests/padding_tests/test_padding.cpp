#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "paracirce/domain.hpp"
#include "paracirce/functions.hpp"
#include "paracirce/padding.hpp"
#include <array>

TEST_CASE("Padding Matern 2D")
{
    using namespace paracirce;

    DiscretDomain<double, 2> D_2D{{{0., 1.0, 101}, {0., 1., 101}}};
    std::array<double, 2> lc{0.64, 0.32};
    double nu = 1.;
    Matern<double, 2> m(lc, nu);

    DiscretDomain<double, 2> expected_dom{{{0., 5.43, 544}, {0., 2.34, 235}}};
    auto dom_pad = padding_estimation(D_2D, m);
    // std::cout << dom_pad << std::endl;
    REQUIRE(dom_pad == expected_dom);
}

TEST_CASE("Padding Matern 3D")
{
    using namespace paracirce;

    DiscretDomain<double, 3> D_3D{{{0., 1., 11}, {0., 1., 11}, {0., 1., 11}}};
    std::array<double, 3> lc{0.4, 0.8, 1.6};
    double nu = 1.;
    Matern<double, 3> m(lc, nu);

    DiscretDomain<double, 3> expected_dom{
        {{0., 2.6, 27}, {0., 6.5, 66}, {0., 15.8, 159}}};
    auto dom_pad = padding_estimation(D_3D, m);
    // std::cout << dom_pad << std::endl;
    REQUIRE(dom_pad == expected_dom);
}

TEST_CASE("Padding Gaussian 2D")
{
    using namespace paracirce;

    DiscretDomain<double, 2> D_2D{{{0., 1., 11}, {0., 1., 11}}};
    std::array<double, 2> lc{0.3, 0.35};
    Gaussian<double, 2> m(lc);

    DiscretDomain<double, 2> expected_dom{{{0., 2.5, 26}, {0., 2.9, 30}}};
    auto dom_pad = padding_estimation(D_2D, m);
    // std::cout << dom_pad << std::endl;
    REQUIRE(dom_pad == expected_dom);
}

TEST_CASE("Padding Gaussian 3D")
{
    using namespace paracirce;

    DiscretDomain<double, 3> D_3D{{{0., 1., 11}, {0., 1., 11}, {0., 1., 11}}};
    std::array<double, 3> lc{0.3, 0.35, 0.8};
    Gaussian<double, 3> m(lc);

    DiscretDomain<double, 3> expected_dom{
        {{0., 2.5, 26}, {0., 3.0, 31}, {0., 6.7, 68}}};
    auto dom_pad = padding_estimation(D_3D, m);
    // std::cout << dom_pad << std::endl;
    REQUIRE(dom_pad == expected_dom);
}
