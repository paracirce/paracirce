set(TEST_NAME "functions")

add_executable(${TEST_NAME} test_functions.cpp)

set_target_properties(${TEST_NAME} PROPERTIES CXX_STANDARD 17 CXX_STANDARD_REQUIRED ON)

include(tests_warnings)

target_link_libraries(${TEST_NAME}
  PRIVATE
  doctest::doctest
  Paracirce::Paracirce)

add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})
