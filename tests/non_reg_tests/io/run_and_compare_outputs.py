#!/usr/bin/env python3

"""
1- Execute the io code with MPI:
mpiexec -n 3 io

The generated GRFs are written into both h5 and txt files.

2- Parse the h5 output files and write GRF values linearly into txt files

3- Compare the content of each transformed file and its txt counterpart

Return 0 if they are identical, 1 if not
"""
import argparse
import subprocess
from pathlib import Path

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('build_dir', help="Directory containing io executable")
    # parser.add_argument('group')
    args = parser.parse_args()

    EXEC_PATH = Path(args.build_dir) / Path("io")
    cmd = "mpiexec -n 3 --oversubscribe " + EXEC_PATH.as_posix()
    subprocess.run(cmd.split())

    H5_FILE_2D = Path(args.build_dir) / Path("field_2D.h5")
    parsing_cmd_2D = "h5ls -d " + H5_FILE_2D.as_posix() + "/field"
    process = subprocess.run(parsing_cmd_2D, capture_output=True, shell=True)
    parsing_cmd_output_2D = process.stdout.decode("utf-8")\
        .split("\n", 2)[2]\
        .replace(" ", "")\
        .replace("\n", "")\
        .replace(",", "\n") + "\n"

    H5_FILE_3D = Path(args.build_dir) / Path("field_3D.h5")
    parsing_cmd_3D = "h5ls -d " + H5_FILE_3D.as_posix() + "/field"
    process = subprocess.run(parsing_cmd_3D, capture_output=True, shell=True)
    parsing_cmd_output_3D = process.stdout.decode("utf-8")\
        .split("\n", 2)[2]\
        .replace(" ", "")\
        .replace("\n", "")\
        .replace(",", "\n") + "\n"
    # with open("formatted_output", "w") as res:
    #     res.write(parsing_cmd_output)

    with open("field_2D.txt") as f_2D:
        with open("field_3D.txt") as f_3D:
            if (f_2D.read() == parsing_cmd_output_2D) and (f_3D.read() == parsing_cmd_output_3D):
                exit(0)
            else:
                exit(1)
